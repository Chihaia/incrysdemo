package test;

import org.assertj.core.api.SoftAssertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.ShopGSMUtils;

import java.util.List;

public class ShopGSMTests {

    public static WebDriver driver;
    public static WebDriverWait wait;

    @Rule
    public TestName name = new TestName();
    public static SoftAssertions softly = new SoftAssertions();

    @Before
    public void initBeforeTest() {

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--incognito");
        options.addArguments("--disable-blink-features");
        options.addArguments("--disable-blink-features=AutomationControlled");
        driver = new ChromeDriver(options);
        wait = new WebDriverWait(driver, 10);
        driver.manage().window().maximize();
        driver.get(ShopGSMUtils.urlWebsite);
    }

    @After
    public void clean() {
        softly.assertAll();
        driver.quit();
    }

    @Test
    public void amazonTest() {
        WebElement searchBox = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("twotabsearchtextbox")));
        searchBox.sendKeys("Smart watch");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("nav-search-submit-button"))).click();
        List<WebElement> prices = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.className("a-price-whole")));
        int lowestPriceFounded = Integer.MAX_VALUE;
        for (WebElement price : prices) {
            System.out.println(price.getText());
            if (lowestPriceFounded > Integer.parseInt(price.getText())) {
                lowestPriceFounded = Integer.parseInt(price.getText());
            }
        }

        System.out.println("LOWEST PRICE: " + lowestPriceFounded);
        for (WebElement price : prices) {
            if (price.getText().equals(String.valueOf(lowestPriceFounded))) {
                price.click();
            }
        }
    }
}
