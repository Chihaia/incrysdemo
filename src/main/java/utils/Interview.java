package utils;

import java.util.ArrayList;
import java.util.List;


public class Interview {
    public static List<Integer> returnIndex(int[] s) {
        List<Integer> indexes = new ArrayList<>();
        for (int i = 0; i < s.length; i++) {
            for (int j = i + 1; j < s.length; j++) {
                if (s[i] + s[j] == 7) {
                    indexes.add(i);
                    indexes.add(j);
                }
            }
        }
        return indexes;
    }

    public static void main(String[] args) {
        int[] numbers = {2, 3, 7, 4, 8};
        System.out.println(returnIndex(numbers));
    }
}
