package utils;

import java.util.*;

public class Test {

    public static void function() {
        System.out.println("test");
    }

    public static int solution(String s1, String s2) {

        HashMap<Character, Integer> charCountS1 = new HashMap<Character, Integer>();
        HashMap<Character, Integer> charCountS2 = new HashMap<Character, Integer>();

        int counter = 0;

        char[] s1String = s1.toCharArray();
        char[] s2String = s2.toCharArray();

        for (char s : s1String) {
            if (charCountS1.containsKey(s)) {
                charCountS1.put(s, charCountS1.get(s) + 1);
            } else {
                charCountS1.put(s, 1);
            }
        }
        for (char s : s2String) {
            if (charCountS2.containsKey(s)) {
                charCountS2.put(s, charCountS2.get(s) + 1);
            } else {
                charCountS2.put(s, 1);
            }
        }

        for (Map.Entry entry : charCountS1.entrySet()) {
            for (Map.Entry entryj : charCountS2.entrySet()) {
                if (entry.getKey().equals(entryj.getKey())) {
                    if (Integer.parseInt(entry.getValue().toString()) > Integer.parseInt(entryj.getValue().toString())) {
                        counter += Integer.parseInt(entryj.getValue().toString());
                    } else {
                        counter += Integer.parseInt(entry.getValue().toString());
                    }
                }
            }
        }

        return counter;

    }

    /*Find sum of first elements from each nested lists that:
    Are even numbers AND
    Have value more than 5
    Examples:
    input: [ [2, 4], [7, 9, 11] ], result: 0
    input: [ [4, 6],  [6, 8] ], result: 6
    input: [ [6, 8],  [8, 6] ], result: 14
    input: [ [8, 6],  [8, 6] ], result: 16
    input: [ [6, 7] ], result: 6
    input: [ [4, 8, 6], [7], [7, 8, 6] ], result: 4 */
    public static int sumOfFirstEvenMoreThan5ValueFromEachList(int[][] array) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < 1; j++) {
                if (array[i][j] % 2 == 0 && array[i][j] > 5) {
                    sum += array[i][j];
                } else {
                    break;
                }
            }
        }
        return sum;
    }

    public static String filteringStrings(String input1, String input2) {
        Map<Character, Integer> charFrequency = new HashMap<>();
        String commonLetters = "";
        List<Character> firstString = new ArrayList<>();
        List<Character> secondString = new ArrayList<>();

        // Converting the strings to ArrayList
        for (int i = 0; i < input1.length(); i++) {
            firstString.add(input1.charAt(i));
        }
        for (int j = 0; j < input2.length(); j++) {
            secondString.add(input2.charAt(j));
        }

        // Finding the common elements between 2 arrays and adding them in a string variable
        for (int i = 0; i < firstString.size(); i++) {
            for (int j = 0; j < secondString.size(); j++) {
                if (firstString.get(i) == secondString.get(j)) {
                    commonLetters += firstString.get(i);
                    secondString.remove(secondString.get(j));
                    break;
                }
            }
        }

        // Converting the String to a charrArray and counting the letter frequency
        char[] commonLettersCharArr = commonLetters.toCharArray();
        for (char s : commonLettersCharArr) {
            if (charFrequency.containsKey(s)) {
                charFrequency.put(s, charFrequency.get(s) + 1);
            } else {
                charFrequency.put(s, 1);
            }
        }

        // Ordering in descending order the HashMap by value
        LinkedHashMap<String, Integer> reverseSortedMap = new LinkedHashMap<>();
        charFrequency.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .forEachOrdered(x -> reverseSortedMap.put(String.valueOf(x.getKey()), x.getValue()));

        // Iterating to reverseSortedMap to return the third expected string
        String finalString = "";
        for (HashMap.Entry entry : reverseSortedMap.entrySet()) {
            finalString += entry.getKey() + entry.getValue().toString();
        }

        return finalString;
    }


    /*Sort Array By Parity Given an array A of non-negative integers, return an array consisting of all the even elements of A,
    followed by all the odd elements of A.

    You may return any answer array that satisfies this condition.

            Example 1:

    Input: [3,1,2,4]

    Output: [2,4,3,1]

    The outputs [4,2,3,1], [2,4,1,3], and [4,2,1,3] would also be accepted.

     */
    public static List<Integer> sort(List<Integer> numbers) {
        List<Integer> finalList = new ArrayList<>();
        for (int i = 0; i < numbers.size(); i++) {
            if (numbers.get(i) % 2 == 0) {
                finalList.add(numbers.get(i));
            }
        }
        for (int i = 0; i < numbers.size(); i++) {
            if (numbers.get(i) % 2 != 0) {
                finalList.add(numbers.get(i));
            }
        }
        return finalList;
    }


/*
    Input1="teste" Input2="teae"
    Output="e2t1"
    Caracterele comune si frecventa lor descrescator.

*/


    public static void main(String[] args) {
        System.out.println(filteringStrings("testesssa", "teaesss"));
        System.out.println(filteringStrings("teste", "teae"));
        List<Integer> listA = new ArrayList<Integer>(Arrays.asList(3, 1, 2, 4));
        List<List<String>> allCookieSettings = Arrays.asList(Arrays.asList("true", "true", "true"),
                Arrays.asList("false", "false", "false"),
                Arrays.asList("true", "false", "false"),
                Arrays.asList("true", "true", "false"),
                Arrays.asList("false", "true", "true"),
                Arrays.asList("false", "false", "true"),
                Arrays.asList("true", "false", "true"),
                Arrays.asList("false", "true", "false"));
        System.out.println(sort(listA));
        int[][] arr = {{4, 2}, {7, 4}, {7, 8, 6}};
        System.out.println(sumOfFirstEvenMoreThan5ValueFromEachList(arr));
    }
}
