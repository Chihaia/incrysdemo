package utils;

public class Pizza {

    public String name;
    public String ingrediente;
    public static int pretIngredient = 3;

    public static int priceCalculator(int basePrice, int ingredinetNumber){
        return basePrice + ingredinetNumber * 3;
    }

    public static String solution(String st) {
        if (checkPalindrome(st)){
            return st;
        }
        String palindromBuild = st.substring(st.length()/2);

        if (checkDuplicatedLetters(st)){
            for (int i = st.length() - 2; i >= 0; i--){
                st += st.charAt(i);
            }
            return st;
        }

        if (st.length() % 2 != 0){
            for (int i = st.length()/2 - 1; i >= 0; i--){
                st += st.charAt(i);
                if (checkPalindrome(st)){
                    return st;
                }
            }
        }
        if (st.length() % 2 == 0 && checkPalindrome(palindromBuild)){
            for (int i = st.length()/2 - 1; i >= 0; i--){
                st += st.charAt(i);
                if (checkPalindrome(st)){
                    return st;
                }
            }
            for (int i = 0; i < st.length(); i++){
                st += st.charAt(i);
                if (checkPalindrome(st)){
                    return st;
                }
            }
        }

        if (st.length() % 2 == 0 && !checkPalindrome(palindromBuild) && st.charAt(0) == 'a' && st.charAt(st.length()-1) == 'a'){
            for (int i = 0; i < st.length(); i++){
                st += st.charAt(i);
                if (checkPalindrome(st)){
                    return st;
                }
            }
        }

        for (int i = st.length() - 2; i >= 0; i--){
            st += st.charAt(i);
            if (checkPalindrome(st)){
                return st;
            }
        }


        return st;
    }

    public static boolean checkPalindrome (String word){
        String palindrome = "";
        for (int i = word.length()-1; i >= 0; i--){
            palindrome += word.charAt(i);
        }
        if (palindrome.equals(word)){
            return true;
        }
        return false;
    }

    public static boolean checkDuplicatedLetters(String word){
        for (int i = 0; i < word.length(); i++){
            for (int j = i + 1; j < word.length(); j++){
                if (word.charAt(i) == word.charAt(j)){
                    return false;
                }

            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println(priceCalculator(10, pretIngredient));
        System.out.println(solution("aaaaaaba"));
    }
}
