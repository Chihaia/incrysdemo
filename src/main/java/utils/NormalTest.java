package utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class NormalTest {

    //extracting the minimum value from the list
    public static Integer getMinValue(List<Integer> list) {
        Integer min = Integer.MAX_VALUE;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) < min) {
                min = list.get(i);
            }
        }
        return min;
    }

    //extracting the maximum value from the list

    public static Integer getMaxValue(List<Integer> list) {
        Integer max = Integer.MIN_VALUE;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) > max) {
                max = list.get(i);
            }
        }
        return max;
    }

    //printing the duplicates from a list

    public static List<Integer> getDuplicates(List<Integer> list) {
        List<Integer> duplicatedItems = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            for (int j = i + 1; j < list.size(); j++) {
                if (list.get(i).equals(list.get(j))) {
                    duplicatedItems.add(list.get(i));
                }
            }
        }
        return duplicatedItems;
    }

    //ordering a list of integers

    public static List<Integer> orderedlist(List<Integer> list) {
        for (int i = 0; i < list.size(); i++) {
            for (int j = i + 1; j < list.size(); j++) {
                if (list.get(i) > list.get(j)) {
                    int temp = list.get(i);
                    list.set(i, list.get(j));
                    list.set(j, temp);
                }
            }
        }
        return list;
    }

    //checking if the word is a palindrome
    public static Boolean isPalindrome(String s) {
        String palindrome = "";
        for (int i = s.length() - 1; i >= 0; i--) {
            palindrome += s.charAt(i);
        }
        return s.equals(palindrome);
    }

    //extracting a substring from a word
    public static String stringExtractor(String A) {
        return A.substring(A.indexOf("Chihaia") + 7, A.indexOf("Florentin"));
    }

    //extracting or printing the odd numbers from a list
    public static List<Integer> oddNumber(List<Integer> list) {
        List<Integer> oddNumbers = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) % 2 != 0) {
                oddNumbers.add(list.get(i));
            }
        }
        return oddNumbers;
    }

    public static HashMap<Character, Integer> countCharOccurrence(String s1) {
        char[] arrayChar = s1.toCharArray();
        HashMap<Character, Integer> countChars = new HashMap<Character, Integer>();

        for (char s : arrayChar) {
            if (countChars.containsKey(s)) {
                countChars.put(s, countChars.get(s) + 1);
            } else {
                countChars.put(s, 1);
            }
        }
        return countChars;
    }




    //

    public static void main(String[] args) {
        List<Integer> listA = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -2, -4, 0, 1001, 999, 2, 3, -2, 1, 1));
    String palindromeExample = "SOTOS";
    String normalString = "ChihaiaSorinFlorentin";
    System.out.println(getMinValue(listA));
    System.out.println(getMaxValue(listA));
    System.out.println(getDuplicates(listA));
    System.out.println(orderedlist(listA));
    System.out.println(isPalindrome(palindromeExample));
    System.out.println(stringExtractor(normalString));
    System.out.println(oddNumber(listA));
    System.out.println(countCharOccurrence("ssseetttttllz"));
    }
}
