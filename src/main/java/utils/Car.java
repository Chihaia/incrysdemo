package utils;

public class Car {
    public String color;

    public void setColor (String color){
        this.color = color;
    }

    public String getColor (){
        return this.color;
    }

    public void make_noise(){

        System.out.println("brrr");
    }

    public void make_noise (String b){
        System.out.println(b);
    }


    public static void main(String[] args) {
        Car objCar = new Car();
        objCar.setColor("red");
        System.out.println(objCar.getColor());
        objCar.make_noise();
        Car objCar2 = new DaciaCar();
        objCar2.make_noise();
        objCar.make_noise("wqd");

    }
}

class DaciaCar extends Car {
@Override
    public void make_noise(){
    System.out.println("DaciaCar");
}
}

